<?php
function right($string,$chars)
{
	$vright = substr($string, strlen($string)-$chars,$chars);
	return $vright;
}

function left($string,$chars)
{
	$vright = substr($string, 0, $chars);
	return $vright;
}

function useable($start, $end, $data) {
	$ret_use = array();
	for( $ii=$start ; $ii<=$end ; $ii++ ) {
		$ret_use[$ii]=False;
	}
	
	$total = $data->num_rows;
	for( $i=0; $i<$total; $i++) {
		$areas = $data->fetch_array( MYSQLI_BOTH );
		for( $jj=$areas["start"];$jj<=$areas["end"];$jj++ ) {
			$ret_use[$jj]=true;
		}
	}
	
	return $ret_use;
}
?>