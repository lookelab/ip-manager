CREATE DATABASE  IF NOT EXISTS `ipman` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `ipman`;
-- MySQL dump 10.13  Distrib 5.6.24, for osx10.8 (x86_64)
--
-- Host: 192.168.0.89    Database: ipman
-- ------------------------------------------------------
-- Server version	5.5.40-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_ipclasses`
--

DROP TABLE IF EXISTS `tbl_ipclasses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_ipclasses` (
  `ipclass` varchar(12) NOT NULL COMMENT 'ip class (MAX C)<xxx.xxx.xxx>',
  `A` varchar(3) DEFAULT NULL COMMENT 'A class <xxx>',
  `B` varchar(3) DEFAULT NULL COMMENT 'B class <xxx>',
  `C` varchar(3) DEFAULT NULL COMMENT 'C class <xxx>',
  `classify` varchar(45) DEFAULT 'private' COMMENT 'network type (public/private)',
  `start` smallint(3) DEFAULT '1' COMMENT 'D Class start addr',
  `end` smallint(3) DEFAULT '255' COMMENT 'D class end addr',
  `gateway` varchar(3) DEFAULT '1' COMMENT 'Gateway IP addr',
  PRIMARY KEY (`ipclass`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_ipclasses`
--

LOCK TABLES `tbl_ipclasses` WRITE;
/*!40000 ALTER TABLE `tbl_ipclasses` DISABLE KEYS */;
INSERT INTO `tbl_ipclasses` VALUES ('192.168.0','192','168','0','private',1,255,'1');
/*!40000 ALTER TABLE `tbl_ipclasses` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-07-22 11:35:02
