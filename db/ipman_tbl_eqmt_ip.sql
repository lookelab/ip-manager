CREATE DATABASE  IF NOT EXISTS `ipman` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `ipman`;
-- MySQL dump 10.13  Distrib 5.6.24, for osx10.8 (x86_64)
--
-- Host: 192.168.0.89    Database: ipman
-- ------------------------------------------------------
-- Server version	5.5.40-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_eqmt_ip`
--

DROP TABLE IF EXISTS `tbl_eqmt_ip`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_eqmt_ip` (
  `ip_cclass` varchar(45) NOT NULL COMMENT 'IP C Class',
  `ip_no` int(11) NOT NULL COMMENT 'ip no',
  `eqmt_name` varchar(45) DEFAULT NULL,
  `ipaddr` varchar(20) DEFAULT NULL COMMENT 'ip address',
  PRIMARY KEY (`ip_cclass`,`ip_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='IP lists';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_eqmt_ip`
--

LOCK TABLES `tbl_eqmt_ip` WRITE;
/*!40000 ALTER TABLE `tbl_eqmt_ip` DISABLE KEYS */;
INSERT INTO `tbl_eqmt_ip` VALUES ('192.168.0',101,'mac mini','192.168.0.101');
/*!40000 ALTER TABLE `tbl_eqmt_ip` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-07-22 11:35:01
