<?php
$cwd = $_SERVER['DOCUMENT_ROOT']."/".explode("/",$_SERVER['REQUEST_URI'])[1];
include_once $cwd.'/includes/global.php';

// get screen width
$SCR_Width = 1280;
// min width of item : 200
$MIN_Num_Width = 35;
$MIN_Name_Width = 117;
// $MIN_IP_Width = 150;
 $MIN_IP_Width = 0;
$MIN_item_Width = $MIN_Num_Width+$MIN_Name_Width+$MIN_IP_Width;
$MIN_HEIGHT = 20;
$COLOR_1 = "#C0C0C0";

function view_ipclass($data, $ipclass) {
	global $SCR_Width, $COLOR_1;
	// C Class
	echo "<table><tr><td width='$SCR_Width'>";
	echo "\t<table cellspacing=0 cellpadding=0 border=1 bordercolor='$COLOR_1' width='400'><tr align='center'><td bgcolor='#FFFF00' width='150'>IP Class</td><td width='150'>";
	$total = $data->num_rows;
	echo "<select name='ipclass' onchange='ref();'>";
	for( $i=0; $i<$total; $i++) {
		$ipi = $data->fetch_array( MYSQLI_BOTH );
		if( $i==0 && $ipclass=="" ) $ipclass = $ipi["ipclass"];
		if( $ipclass == $ipi["ipclass"] ) {
			$sel="selected";
			$ip_cclass = $ipi["A"].".".$ipi["B"].".".$ipi["C"];
		}
		else $sel = "";
		if( $ipi["classify"]=="public") $classify="*";
		elseif( $ipi["classify"]=="private") $classify="+";
		elseif( $ipi["classify"]=="subnet") $classify="&nbsp;&nbsp;-";
		echo "\t<option ".$sel." value='".$ipi["ipclass"]."'>".$classify.$ipi["descs"]."</option>"; 
	}
// 	echo $CCLASS_ip;
	echo "</select>";
	echo "\t</td>";
	echo "\t<td align='center' width='70'>$ip_cclass";
	echo "\t</td></tr></table>";
	echo "</td></tr></table>";
// 	$data->free();
	return $ipclass;
}


function view_iptable($data, $ipclass, $useables) {
	global $SCR_Width, $MIN_item_Width, $MIN_Name_Width, $MIN_Num_Width, $MIN_HEIGHT, $COLOR_1, $HLLOC;
	$COL_index = floor( $SCR_Width / $MIN_item_Width );
	$ROW_index = ceil( 255 / $COL_index );
	echo "<table cellspacing=1 cellpadding=0>";
	echo "<tr>";
	// Title
	for ($j = 0; $j < $COL_index; $j++) {
		echo "<td width='$MIN_item_Width'>";
		echo "<table cellspacing=0 cellpadding=0 border=1 bgcolor='#FFFF00' bordercolor='$COLOR_1' width='$MIN_item_Width'><tr align='center'><td width='$MIN_Num_Width'>No.</td><td width='$MIN_Name_Width'>HostName</td></tr></table>";
		echo "</td>";
	}
	echo "</tr>";

	// Fetch data
	$LIST_name = array();
	$LIST_ip = array();
	for ($k = 1; $k < 256; $k++) {
		$LIST_name[$k] = "";
		$LIST_ip[$k] = "";
	}
	$total = $data->num_rows;
	for($i=0;$i<$total;$i++) {
		$ipi=$data->fetch_array(MYSQLI_BOTH);
		$LIST_name[$ipi["ip_no"]] = $ipi["eqmt_name"];
		$LIST_ip[$ipi["ip_no"]] = $ipi["ipaddr"];
	}
	
	// Data
	for ($i = 0; $i < $ROW_index; $i++) {
		echo "<tr>";
		for ($j = 0; $j < $COL_index; $j++) {
			$CUR_index = $i + $j*$ROW_index +1;
			if( $CUR_index>255 ) continue;
			
			// Useables
			if( $useables[$CUR_index] ) {
				echo "<td width='$MIN_item_Width' height='$MIN_HEIGHT'>\n";
				echo "<table cellspacing=0 cellpadding=0 border=1 bordercolor='$COLOR_1' width='$MIN_item_Width' height='$MIN_HEIGHT'>";
				echo "<tr height='$MIN_HEIGHT'>";
				echo "<td align='center' width='$MIN_Num_Width'>".$CUR_index."</td>";
				if( $LIST_name[$CUR_index]=="" ) {
					echo "<td align='left' width='$MIN_Name_Width'>";
					echo "<input type='text' name='enm_$CUR_index' onkeydown='checkInput(event,this);' maxlength='16' style='width:100%;height:$MIN_HEIGHT;padding:0;margin:0;border:0;outline:none'>";
				} else {
					echo "<td width='$MIN_Name_Width' onclick=\"if(confirm('Are you Sure to remove [ $LIST_name[$CUR_index] ] ?')) {worker.location='$HLLOC/manage/remove/?ipaddr=$LIST_ip[$CUR_index]';}\">";
				}
// 				echo "<a href='http://".$LIST_ip[$CUR_index]."' target='".$CUR_index."'>".$LIST_name[$CUR_index]."</a>";
				echo $LIST_name[$CUR_index];
				echo "</td>";
				echo "</tr></table>\n";
				echo "</td>\n";
			} else {
				echo "<td width='$MIN_item_Width' height='$MIN_HEIGHT' bgcolor='$COLOR_1'>\n";
				echo "</td>\n";
			}
		}
		echo "</tr>";
	}
	echo "</table>";
}