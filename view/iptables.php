<?php
$cwd = $_SERVER['DOCUMENT_ROOT']."/".explode("/",$_SERVER['REQUEST_URI'])[1];
include_once $cwd.'/includes/global.php';
include_once $HPLOC.'/includes/dbconnect.php';
include_once $HPLOC.'/includes/requests.php';
include_once $HPLOC.'/includes/funcs.php';
include_once $HPLOC.'/dbaccess/ipaddr.php';

include_once $HPLOC.'/htdocs/ivfunc.php';
include_once $HPLOC.'/htdocs/header.html';

?>
<script>
function ref(){
	var loc;
	loc = "?ipclass="+document.all.ipclass.value;
	location.href=loc;
}

function checkInput(event, inp) {
	if( event.keyCode == 13 ) {
		if( inp.value.length > 0 ) {
			if( confirm("Are you Sure to ADD [ "+inp.name.substr(4, inp.name.length-4)+" : "+inp.value+" ] ?") ) {
				worker.location="<?=$HLLOC?>/manage/add/?ipclass="+document.all.ipclass.value+"&ipno="+inp.name.substr(4, inp.name.length-4)+"&enm="+inp.value;
			}
		}
	} else if( event.keyCode == 27 ) {
		inp.value="";
	}
}
</script>
	<style>
		td {font-size:9pt; 
			text-align:center; 
		}
	</style>

<?php

// C class
$data = select_ipclass($connect);
$ipclass = view_ipclass($data, $ipclass);

$data = select_ipaddr($ipclass, $connect);

$data2 = select_useable($connect, $ipclass);
$useable = useable(1,255,$data2);

view_iptable($data, $ipclass, $useable);
?>
<iframe name='worker' width='0' height='0' style='display:none'></iframe>